//Alejandro Garay López
// 24/04/2021

import java.util.ArrayList;

public class PruebaListaEnlazada {

	public static void main(String[] args) {
		
		ArrayList listaCompra = new ArrayList();
		listaCompra.add("Pan");
		listaCompra.add("Colacao");
		listaCompra.add("Café");
		listaCompra.add("Agua");
		listaCompra.add("Galletas");
		System.out.println("Lista de la compra:");
		for (int i = 0; i < listaCompra.size(); i++) {
			System.out.println(listaCompra.get(i));
		}
		System.out.println("elementos en la lista: " + listaCompra.size());
		System.out.println("elementos 3 en la lista: " + listaCompra.get(3));
		System.out.println("posición del elemento Miel: " + listaCompra.indexOf("Pan"));
		System.out.println("eliminado: " + listaCompra.remove("Pan"));
	}
}